"""
Chapter Two
Input, Processing and Output
"""

# Input example
# variable = input(prompt)
user_input = input("Please enter your name: ")

# Convert data type of input example
# int(item) converts item to int
age = int(input("Please enter your age: ")) # 10
# float(item) converts item to float
cake = float(input("How much of the cake is left? ")) # 0.5

# Math examples
# + addition
a = a + a
# - subtraction
b = b - b
# * multiplication
c = c * c
# // integer division
d = d // d
# / floating point division
e = e / e
# % modular or remainder
f = f % f
# ** exponent
x = x ** x # x^x

# Breaking statements onto multiple lines with continuation character (\)
result = var1 * 2 + var2 * 3 + \
         var3 * 4 + var4 * 5

# Magic number example
amount = balance * 0.069

# Named constants examples
INTEREST_RATE = 0.069

# Turtle example
import turtle
turtle.forward(100)
turtle.left(90) # turn left 90 degrees
turtle.right(90) # turn right 90 degrees

turtle.pendown() # draws while moving
turtle.penup() # does not draw while moving

turtle.circle(100) # draws circle

turtle.reset() # erases all drawings and resets everything except background colour
turtle.clear() # erases all drawings and does not change anything else
turtle.clearscreen() # erases all drawings and resets everything

turtle.hideout() # hides turtle
turtle.showturtle() # shows turtle

turtle.write("insert text") # display text in graphics window

turtle.begin_fill() # fill a shape with colour
turtle.end_fill() # once this command is run shape will be filled with colour

turtle.done() # stops graphics window from closing

# If statements example
if test_word == "test":
    print("true")
elif test_word != "test":
    print("false")

# Boolean expressions and relational operators example
x > y

x < y

x >= y

x <= y

x == y

x != y

# Logical operators example
if test_word == "test" and test_word_two != "test_two": # and operator
    print("something")
elif test_word == "test" or test_word_two == "test_two": # or operator
    print("something")
else test_word not in test_word_two: # not operator
    print("something")
'''
NOTES:

- input function always returns a string

- input does not automatically display a space after prompt

- type conversion only works on numeric values, else throws error

- / performs floating point division

- // performs integer division

- ** raises value to a power

- % gets the remainder (typically used to convert times and distances and to detect odd or even numbers

- PEMDAS or BODMAS order of operation

- two ints = int

- two floats = float

- int and float = float

- type conversion of float to int causes truncation of fractional part

- any part of a statement inside parentheses can be broken without the continuation character

- special characters in string literal (\n = newline), (\t = tab)

- magic numbers are unexplained numeric values that appears in a programs code

- to use turtle graphics system you must import the turtle module (import turtle)

- the turtles initial heading is 0 degrees (east)

- it is possible to have a block inside another block

- compared characters are based on the ASCII values for each character

- indentation is important when nesting statements
'''