"""
Chapter Four
Repetition & Structures
----
Chapter Five
Functions
"""

# While loop example
while 4 < 5: # while condition is true, do something
    print("something")
    break

# Count-controlled loop example
for num in [1, 2, 3, 4, 5]:
    print num

for name in ["Winken", "Blinken", "Nod"]:
    print name

# Range function in for loop
for x in range(1,5, 1): # (start, end, step)
    print x

# Augmented assignment operators
x += 5 # x = x + 5

y -= 2 # y = y - 2

z *= 10 # z = z * 10

a /= b # a = a / b

c %= 3 # c = c % 3

# Function example
def function1():
    print("something")

# Import example
import module_name

# dot notation Example
module_name.function_name()

'''
NOTES:

- PEP8 - style guide for python code

- while loops conditions are tested for a true or false value

- while loops statements are repeated for as long as the condition is true

- range returns iterable object

- range characteristics: one arg = ending limit, two arg = start and end limit, three arg = third arg is step value

- the range function can be used to generate a sequence with numbers in descending order

- the benefits of using functions - simpler code, code reuse, better testing and debugging, faster development, easier facilitation of teamwork

- function naming rules: cannot use key words as function name
                         cannot contain spaces
                         first character must be a letter of underscore
                         all other characters must be a letter, number or underscore
                         uppercase or lowercase characters are distinct
                         
- function names should be descriptive of the task carried out by the function

- function header: first line of function

- block: set of statements that belong together as a group

- main function: called when program starts 

- each block must be indented

- blank lines that appear in a block are ignored

- local variable: variable that is assigned a value inside a function

- scope: the part of a program in which a variable may be accessed

- local variables cannot be accessed by statements inside its function which precede its creation

- different functions may have local variables with the same name

- python allows writing a function that accepts multiple arguments

- arguments are passed by position to corresponding parameters

- changes made to a parameter value within the function do not affect the argument

- keyword argument: argument that specifies which parameter the value should be passed to

- To write a value-returning function, you write a simple function and add one or more return statements

- standard library: library of pre-written functions that comes with python

- modules: files that stores functions of the standard libary

- to call a function stored in a module, need to write an import statement

- random module: includes library functions for working with random numbers

- dot notation: notation for calling function belonging to a module

- random.seed() to specify a desired seed value

- math module: part of standard library that contains functions that are useful for performing mathematical calculations

- modularization: grouping related functions in modules

- module is a file that contains python code

- rules for modules names: 
                          file name should end in .py
                          cannot be the same as a python keyword

'''