"""
REST API
"""

'''
NOTES

- API: Application Program Interface

- REST: Representational State Transfer

- introduced by Dr. Roy Fielding in 2000

- not a standard

- stateless: request from client to server must contain all of the information necessary to understand the request

- cacheable: clients can cache responses. responses must define themselves as cacheable or not

- layered system: client cannot ordinarily tell whether it is connceted directly to the end server

- uniform interface: fundamental to the design of any REST service
    identification of resources
    manipulation of resources
    self-descriptive messages
    HATEOAS
    
- web service APIs that adhere to the REST architectural constraints are called RESTful

- identification of resources via RESTful URI

- protocol identifies the transport scheme

- host name identifies the server address

- path and query string can be used to identify and customise the accessed resource

- the key abstraction of information in REST is a resource

- a resource is a conceptual mapping to a set of entities

- represented with a global identifier (URI in HTTP)

- POST - create

- GET - retrieve

- PUT - update

- DELETE - delete

- HATEOAS: Hypermedia As The Engine of Application State: key to make surfing the web with a browser possible
'''