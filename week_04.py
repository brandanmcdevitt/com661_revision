"""
Chapter Ten
Classes and OOP
"""

# Class definition
import random
class Coin:
    def __init__(self):
        self.sideup = 'Heads'

    def toss(self):
        if random.randint(0,1) == 0:
            self.sideup = 'Heads'
        else:
            self.sideup = 'Tails'

    def get_sideup(self):
        return self.sideup

# Create new instance of class
my_instance = Coin()

# Call class methods
my_instance.sideup()

'''
NOTES:

- procedural programming: writing programs made of functions that perform specific tasks

- focus: to create procedures that operate on the programs data

- object orientated programming: focused on creating objects

- object: entity that contains data and procedures

- encapsulation: combining data and code into a single object

- data hiding: objects data attributes are hidden from code outside the object

- object reusability: the same object can be used in different programs

- data attributes: define the state of an object

- public methods: allow external code to manipulate the objects

- private methods: used for objects inner workings

- class: code that specifies the data attributes and methods of a particular type of object

- instance: an object create from a class

- class definition: set of statements that define a classes method and data attributes

- initialzer method: automatically executed when an instance of the class is created

- two underscores in front of attribute make it private

- objects state: the values of the objects attribute at a given moment

'''