"""
Version Control
"""

'''
NOTES

- version control: revision control or source control
                   a history of changes to documents, codes, large websites
                   changes are usually identified by version number
                   
- types of version control: git
                            mercurial (hq)
                            bazaar
                            subversion (svn)
                            concurrent version system (cvs)
                            perforce
                            visual source safe

- git commands: add
                bisect
                branch
                checkout
                clone
                commit
                diff
                fetch
                grep
                init
                log
                merge
                mv
                pull
                push
                
- git head: last commit

- git index: staged files

- git workflow: init or clone - git init
                edit files
                stage changes - git add
                review changes - git status
                commit changes - git commit
                
- git init will create .git folder
'''