"""
Chapter Six
Files and Exceptions
----
Lists
"""

# Open a file
file_object = open("file.txt", 'r') # open file in read mode
file_object = open("file.txt", 'w') # open file in write mode
file_object = open("file.txt", 'a') # open file in append mode

# Write to a file
file_object.write("text")

# Close the file
file_object.close()

# Exception handling
try:
    insert statement here
except:
    insert statement here

# List format
list = [item1, item2, item3]

# Return length of list
size = len(list)

# Concatenate two lists
list1 = [1, 2, 3, 4]
list2 = [5, 6, 7, 8]
list3 = list1 + list2

# List slicing format
numbers = [1, 2, 3, 4, 5]
print(numbers[1:3]) # numbers[start : end]

# Dictionaries
dictionary = {key1 : val1, key2 : val2}

# Retrieve a value from a dictionary
print(dictionary[key1])

# Delete key value pair
del dictionary[key1]

'''
NOTES:

- when a program uses a file there are 3 steps: open the file
                                                process the file
                                                close the file
                                                
- read method: file obkect method that reads entire file contents into memory

- readline method: file object method that reads a line from the file

- exception: error that occurs while program is running

- traceback: error message that gives information regarding line numbers that caused the exception

- some exceptions can be avoided using: input validation

- some exceptions can not be avoided: trying to convert non-numeric string to an int
                                      trying to open for reading a file that doesnt exist
                                      
- exception handler: code that responds when exceptions are raised and prevents program from crashing

- list: an object that contains multiple data items

- element: an item in a list

- index: a number specifying the position of an element in a list

- indexError: raised if invalid index is used

- concatenate: join two things together

- the + operator can be used to concatenate two lists

- slice: a span of items that are taken from a sequence

- dictionary: object that stores a collection of data (key, value pairs)

'''